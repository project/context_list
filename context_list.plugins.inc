<?php
/**
 * @file
 * Plugins
 */

/**
 * Base class for displaying context conditions
 */
class ContextListConditionDisplay {

  /**
   * The condition key that is being displayed
   * @var string
   */
  public $condition = NULL;

  /**
   * The settings for this condition
   * @var array
   */
  public $settings = NULL;

  /**
   * The constructor for this class
   *
   * @param string $condition
   *   The condition identifier
   * @param array $settings
   *   Array of settings
   */
  public function __construct($condition = NULL, $settings = array()) {
    $this->condition = $condition;
    $this->settings = $settings;
  }

  /**
   * Display the condition
   *
   * @return string
   *   The condition
   */
  public function display() {
    return $this->condition;
  }
}

/**
 * A condition display class for displaying conditions that have a
 * values key in the settings.
 *
 * This class is used by a couple of different conditions.
 */
class ContextListConditionDisplayValues extends ContextListConditionDisplay {
  /**
   * Display the condition
   */
  public function display() {
    if (isset($this->settings['values']) && is_array($this->settings['values'])) {
      $values = $this->settings['values'];
      $values = array_map('htmlentities', $values);
      return $this->condition . ': ' . implode(', ', $values);
    }

    parent::display();
  }
}

/**
 * The base class for outputting context reactions
 */
class ContextListReactionDisplay {
  /**
   * The reaction key for this reaction
   * @var string
   */
  public $reaction = NULL;

  /**
   * The settings array for this reaction
   * @var array
   */
  public $settings = NULL;

  /**
   * The constructor for this reaction display
   *
   * @param string $reaction
   *   The context's reaction
   * @param array $settings
   *   The context reaction settings
   */
  public function __construct($reaction = NULL, $settings = array()) {
    $this->reaction = $reaction;
    $this->settings = $settings;
  }

  /**
   * Display the context reaction
   * @return string
   *   The reaction representation
   */
  public function display() {
    $d = $this->reaction . ": ";
    if (is_array($this->settings)) {
      $d .= $this->implodeSettings($this->settings);
    }
    else {
      $d .= htmlentities($this->settings);
    }
    return $d;
  }

  /**
   * Recursively implodes a context reaction settings array.
   *
   * @param array $settings
   *   An array of context reactions.
   *
   * @return string
   *   A human readable string.
   */
  function implodeSettings($settings) {
    $string = '';
    foreach ($settings as $key => $setting) {
      if (is_array($setting)) {
        $string .= $key . ': [' . $this->implodeSettings($setting) . ']';
      }
      else {
        // default to listing out
        return implode(', ', array_map('htmlentities', array_filter($settings)));
      }
    }
    return $string;
  }

}

/**
 * Implements hook_context_list_register_condition_display().
 */
function context_list_context_list_register_condition_display() {
  return array(
    'all' => 'ContextListConditionDisplay',
    'defaultcontent' => 'ContextListConditionDisplay_defaultcontent',
    'path' => 'ContextListConditionDisplay_path',
  );
}

/**
 * Implements hook_context_list_register_reaction_display().
 */
function context_list_context_list_register_reaction_display() {
  return array(
    'all' => 'ContextListReactionDisplay',
    'block' => 'ContextListReactionDisplay_block',
    'region' => 'ContextListReactionDisplay_region',
    'metatag' => 'ContextListReactionDisplay_metatag',
  );
}
