<?php
/**
 * The reaction display class for metatag
 */
class ContextListReactionDisplay_metatag_context_reaction extends ContextListReactionDisplay {
  /**
   * Display reaction
   */
  public function display() {
    $reaction_details = array();

    $tags = array_filter($this->settings['metatags']);
    foreach ($tags as $tag => $details) {
      $this_tag = check_plain($tag) . ': ' . check_plain($details['value']);
      if ($details['value'] != $details['default']) {
        $this_tag .= ' (default: ' . check_plain($details['default']) . ')';
      }
      $reaction_details[] = $this_tag;
    }

    if (!$tags) {
      $reaction_details[] = 'No tags';
    }

    $list = array(
      '#theme' => 'item_list',
      '#items' => $reaction_details,
    );
    $rendered_list = '<p>Metatag:</p>' . drupal_render($list);
    return $rendered_list;
  }

}
