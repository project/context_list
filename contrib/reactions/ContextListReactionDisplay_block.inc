<?php
/**
 * The reaction display class for blocks
 */
class ContextListReactionDisplay_block extends ContextListReactionDisplay {
  /**
   * Display reaction
   */
  public function display() {
    $reaction_details = array();

    // check if anything wants to modify the list of blocks
    drupal_alter('context_list_reaction_blocks', $this->settings['blocks']);
    foreach ($this->settings['blocks'] as $block => $details) {

      // see if anythings wants to modify the block name
      drupal_alter('context_list_reaction_block_name', $block, $details);

      $block_link = $details['module'] . ': ' .
        l($block, 'admin/structure/block/manage/' . $details['module'] . '/' . $details['delta'] . '/configure');
      // if we know the link structure for this block, link to the edit screen
      switch ($details['module']) {
        case 'menu':
          $block_link .= ' (' . l(t('Edit'), 'admin/structure/menu/manage/' . $details['delta']) . ')';
          break;

        case 'views':
          $block_delta = $details['delta'];
          // hashed views blocks support: replace hash by real delta
          if (strlen($block_delta) >= 32) {
            $hashes = variable_get('views_block_hashes', '');
            if (isset($hashes[$block_delta])) {
              $block_link = str_replace($block_delta, $hashes[$block_delta], $block_link);
              $block_delta = $hashes[$block_delta];
            }
          }
          $view = str_replace('-', '/', $block_delta);
          $block_link .= ' (' . l(t('Edit'), 'admin/structure/views/view/' . $view) . ')';
          break;
      }

      array_push(
        $reaction_details,
        $block_link
      );
    }
    $list = array(
      '#theme' => 'item_list',
      '#items' => $reaction_details,
    );
    $rendered_list = '<p>Blocks:</p>' . drupal_render($list);
    return $rendered_list;
  }

}
